#!/bin/bash
################################################
#fpbinfopw.sh 
#password encryption, decryption.
#Script is not finish!
superpw=p
fpbinfopwfile=xy
Encrypt()
	{
	openssl enc -e -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -pass pass:$superpw -salt -out ${fpbinfopwfile}.enc
	}
	
Decrypt()
	{
	openssl enc -d -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -pass pass:$superpw -salt -in ${fpbinfopwfile}.enc
	}
	
Testi() 
{
echo "$* xyz"
}	

Suchepw()
	{
	
	for i in `Decrypt`; do
		pwfound=`echo "$i" | grep "^${1}:"  | cut -d":" -f2`
		if [ -n "$pwfound" ]; then
			#echo "$pwfound"
			pwgefunden="$pwfound"
		fi
	done
	if [ -z "$pwgefunden" ]; then
		echo "Kein PW gefunden fuer \"^${1}:\""
		exit 1
	else
		echo "$pwgefunden"
	fi
	}

Addkeyname()
	{
	if [ -f "${1}" ]; then
		keynameneu=`cat "${1}" `
	else
		keynameneu=`echo "${1}" `
	fi
	for keynamecheckin in $keynameneu; do
		keynameexist=unknown
		keynamecheckist=`echo $keynamecheckin | cut -d":" -f1 `
		for i in `Decrypt`; do
			keynamefound=`echo "$i" | cut -d":" -f1 | grep "^${keynamecheckist}$"  `
			if [ -n "$keynamefound" ]; then
				keynameexist=yes
			fi
		done
		if [[ "$keynameexist" == "yes" ]]; then
			echo "Gibt es schon: $keynamefound"
		else
			varneu="$keynamecheckin $varneu"
		fi
	done
	varalt=$(for i in `Decrypt`; do echo "$i" ; done )
	echo "$varalt $varneu" | `Encrypt`
		
	}

pwchange()
		{
		if [ -f "${1}" ]; then
		keynameneu=`cat "${1}" `
	else
		keynameneu=`echo "${1}" `
	fi
	for keynamecheckin in $keynameneu; do
		keynameexist=unknown
		keynamecheckist=`echo $keynamecheckin | cut -d":" -f1 `
		keynamecheckpw=`echo $keynamecheckin | cut -d":" -f2 `
		pwaendern=nein
		for i in `Decrypt`; do
			keynamefound=`echo "$i" | cut -d":" -f1 | grep "^${keynamecheckist}$"  `
			keynamefoundpw=`echo "$i" | cut -d":" -f2  `
			
			if [ -n "$keynamefound" ]; then
				keynameexist=yes
				keynamefor=$keynamefound
				if [[ "$keynamefoundpw" != "$keynamecheckpw" ]]; then
					pwaendern=ja
					pwalt=$i
					pwneu=$keynamecheckpw
					
				fi
			fi
		done
		if [[ "$keynameexist" == "yes" ]]; then
			if [[ "$pwaendern" == ja ]]; then
				echo "$keynamefor , PW Aendern"
				Decrypt | sed -e "s/${keynamefor}:${pwalt}/${keynamefor}:${pwneu}/g" | Encrypt
				#for Eintrag in `Decrypt`; do echo "$Eintrag" ; done | sed -e "s/${keynamefor}:${pwalt}/${keynamefor}:${pwneu}/g" | Encrypt
			else
				echo "$keynamefor , PW Aendern nicht noetig"
			fi
		fi
	done
	#for i in `Decrypt`; do echo "$i" ; done 
	#echo "$varalt $varneu" | `Encrypt`
		}
		
showallkeyname()
	{
	for i in `Decrypt`; do echo "$i" | cut -d":" -f1 ; done
	}
	
if [ "$1" == "-s" ] && [ -n "$2" ]; then
	Suchepw $2
elif [ "$1" == "-a" ] && [ -n "$2" ]; then
	Addkeyname $2
elif [ "$1" == "-show" ] ; then
	showallkeyname
elif [ "$1" == "-r" ] && [ -n "$2" ] ; then
	pwchange $2
else
	echo "In Work, script is not finish!"
	echo "fpbinfopw.sh -a <keyname>:<password> # Add"
	echo "fpbinfopw.sh -s <keyname> # show password"
	echo "fpbinfopw.sh -show # show all keyname"
	echo "fpbinfopw.sh -r <keyname>:<password> # Change password from keyname"
fi
