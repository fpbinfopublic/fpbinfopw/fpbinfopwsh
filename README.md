# fpbinfopwsh

fpbinfopw.sh script

for more info:
https://fpb-info.de/index.php/de/eigene-tools/bash-fpbinfopw-sh

Alle nötigen Dateien werden erzeugt wenn diese fehlen.

Einmalig ./fpbinfopwsh -m ausfuehren, damit wird dann auch die Passwortdatei erzeugt, falls diese noch nicht vorhanden ist.

Bei der Erstausfuehrung oder wenn in ein Script am anderen Ort dieses Script einbgebunden wurde, werden einige Fragen gestellt:

1. Pfad angeben. Muss man nicht, dann wird dort die Dateien abgelegt wo das Script aufgerufen wird.
Besser mit Pfad. Selbst wenn dann in verschiedenen Scripte eingebunden wird und diese Script an unterschiedlichen Orten liegt, wird immer die selbe Konfiguration und Passwortdatei verwendet.

2. Falls in diesen Pfad nicht die benoetigten Dateien liegen, werden diese Erzeugt.

3. Falls Passwortdatei fehlt, wird diese neu erstellt.

Dateien die erstellt werden:

fpbinfopw.sh.cfg
Nur in der Datei fpbinfopw.sh.cfg Änderungen vornehmen.
Kann ueber Scipt auch angepasst werden das auch ein Backup erstellt.
Falls nicht mehr funktioniert, einfach löschen, dann wird wieder erstellt oder das Backup nutzen sofern vorhanden.

fpbinfopw.sh.pfad.cfg
Bei fpbinfopw.sh.pfad.cfg  kann der Pfad angepasst werden.

fpbinfopwAutoMenu.cfg
Wird automatisch erstellt, Änderungen fuehren evtl zu Fehler (noch nicht getestet), wird immer wieder ueberschrieben.

Info: Es gibt bei Menu "Script pruefen", da folgendens bei einer Frage eintragen: d8117e332e4f449056ffc4944a1c0b87 first and old
cac35c8952c90e2b6833828f8e25fd5a 02.06.2018
